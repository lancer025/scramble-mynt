package med.scramble;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

/**
 * Created by lancer on 5/8/16.
 */
public class GridViewAdapter extends BaseAdapter {

    //Imageloader to load images
    private ImageLoader imageLoader;

    //Context
    private Context context;

    //Array List that would contain the urls and the titles for the images
    private ArrayList<String> images;
    private ArrayList<String> names;

    public GridViewAdapter (Context context, ArrayList<String> images, ArrayList<String> names){
        //Getting all the values
        this.context = context;
        this.images = images;
        this.names = names;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return images.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Creating a linear layout
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        //NetworkImageView
        NetworkImageView networkImageView = new NetworkImageView(context);

        //Initializing ImageLoader
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        String uri=images.get(position);
        if(MainActivity.lruCache.get(uri) == null) {
            imageLoader.get(images.get(position), ((MainActivity) context).getImageListener(networkImageView, R.mipmap.ic_launcher, android.R.drawable.ic_dialog_alert,uri));
        }else{
            networkImageView.setImageBitmap(MainActivity.lruCache.get(uri));
            Log.d("volley", "image already in cache in adapter on position "+ position);


        }

        //Setting the image url to load
        networkImageView.setImageUrl(images.get(position),imageLoader);

        //Creating a textview to show the title
        TextView textView = new TextView(context);
//        textView.setText(names.get(position));

        //Scaling the imageview
        DisplayMetrics metrics = new DisplayMetrics();
//        ((MainActivity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
//
//        int width = metrics.widthPixels;
//        int height = metrics.heightPixels;
//        networkImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        int idealChildHeight = (int) ((height-50*3)/3);
        networkImageView.setLayoutParams(new GridView.LayoutParams(200, 200));


        //Adding views to the layout
        linearLayout.addView(textView);
        linearLayout.addView(networkImageView);

        //Returnint the layout
        return linearLayout;
    }
}
