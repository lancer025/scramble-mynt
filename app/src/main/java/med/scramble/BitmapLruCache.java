
package med.scramble;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.toolbox.ImageLoader.ImageCache;

public class BitmapLruCache extends LruCache<String, Bitmap> implements ImageCache {
	
	// Singleton instance of LRU cache.
    private static BitmapLruCache mInstance;
    
    private BitmapLruCache() {
        super(getDefaultLruCacheSize());
    }



   /* public BitmapLruCache(int maxSize) {
        super(maxSize);
    }*/

    @Override
    public Bitmap getBitmap(String url) {
        return get(url);
    }

    @Override
    public void putBitmap(String url,
            Bitmap bitmap) {
        put(url, bitmap);
    }
    
    /**
     * Opens LRU cache
     * 
     * @return single instance of LRU cache.
     */
    public static synchronized BitmapLruCache open() {
        if (mInstance == null) {
            mInstance = new BitmapLruCache();
        }
        return mInstance;
    }
    
    /**
     * Get default cache size that is 1/8th of run time memory of application.
     * 
     * @return
     */
    private static int getDefaultLruCacheSize() {
        // Get maximum memory available to application in KB.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        // Set cache size to 1/8th of maximum memory.
        final int cacheSize = maxMemory / 8;
        return cacheSize;
    }
}
