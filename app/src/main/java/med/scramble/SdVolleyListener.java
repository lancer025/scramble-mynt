package med.scramble;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


/**
 * Created by lancer on 06/08/16.
 */
public class SdVolleyListener  implements Response.ErrorListener , Response.Listener<String> {



    SAXParserFactory factory = SAXParserFactory.newInstance();
    private static int selected_item, try_count=0;


    SAXParser saxParser ;//= factory.newSAXParser();
    MainActivity mactivity;
    SdVolleyListener(MainActivity activity){
        mactivity=activity;
        try {
            saxParser = factory.newSAXParser();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d("Error.Response", error.getMessage());
        mactivity.stopProgress();
    }

    @Override
    public void onResponse(String response) {

        Log.d("Response", response.toString());
        InputStream is=null;



        try {
            is = new ByteArrayInputStream(response.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            saxParser.parse(is,mactivity.handler);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
