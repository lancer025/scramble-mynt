package med.scramble;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by lancer on 06/08/16.
 */

public class MainActivity extends AppCompatActivity {
    private  Context context;
    GridView gridview;
    ProgressDialog loading;
    AlertDialog dialog;
    Handler handlerThread;
    static final String[] num = new String[]{
            "A", "B", "C", "D", "E", "F", "G", "H", "I"
    };

    public static final String DATA_URL = "http://www.simplifiedcodingreaders.16mb.com/superheroes.php";

    //Tag values to read from json
    public static final String TAG_IMAGE_URL = "image";
    public static final String TAG_NAME = "name";

    //GridView Object
    private GridView gridView;

    //ArrayList for Storing image urls and titles
    private ArrayList<String> images;
    private ArrayList<String> names;
    private ArrayList<Integer> exclusions;


    private  int selected_item, try_count = 0, image_load_count=0;
    SdVolleyListener listener;
    final Random rnd = new Random();
    static BitmapLruCache lruCache=null;
    TextView timer, timerText;
    Button restart_btn;
    Boolean shown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ArrayAdapter<String>ss_adap = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, num);
//        gridview.setAdapter(ss_adap);
//        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(MainActivity.this, ((TextView)view).getText(), Toast.LENGTH_SHORT).show();
//            }
//        });
        context = this;
        gridView = (GridView) findViewById(R.id.image_grid);
        timer = (TextView)findViewById(R.id.timer);
        timerText = (TextView)findViewById(R.id.timerText);
        restart_btn = (Button) findViewById(R.id.restart);
        shown=false;

        images = new ArrayList<>();
        names = new ArrayList<>();
        exclusions = new ArrayList<>();
        listener = new SdVolleyListener(this);
        lruCache = BitmapLruCache.open();

        //laod grid data

        getData();

        // click on gridview

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(try_count!=0){

                    if (selected_item == position) {
                        LinearLayout ll = (LinearLayout) gridView.getChildAt(position);

                        ImageLoader imageLoader;
                        ll.removeViewAt(1);
                        NetworkImageView networkImageView = new NetworkImageView(context);
                        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
                        String uri=images.get(position);
                        if(lruCache.get(uri) == null) {
                            imageLoader.get(uri, getImageListener(networkImageView, R.mipmap.ic_launcher, android.R.drawable.ic_dialog_alert,uri));
                        }else{
                            Log.d("volley","image already in cache in grid item click");
                            networkImageView.setImageBitmap(lruCache.get(uri));

                        }
                        //Setting the image url to load
                        networkImageView.setImageUrl(images.get(position), imageLoader);
                        networkImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        networkImageView.setLayoutParams(new GridView.LayoutParams(200, 200));
                        ll.addView(networkImageView);
                        if(exclusions.size()<9)
                            showImage();
                        else {
                            Toast.makeText(MainActivity.this, "Congrats !! Game's over", Toast.LENGTH_SHORT).show();
                            timer.setText("Game's Over");
                            restart_btn.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Sorry wrong selection", Toast.LENGTH_SHORT).show();
                    }
                } else {
                }
            }
        });

        restart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exclusions.clear();
                names.clear();
                images.clear();
                shown=false;
                images = new ArrayList<>();
                names = new ArrayList<>();
                timer.setText("");
                exclusions = new ArrayList<>();
                timerText.setVisibility(View.VISIBLE);
                listener = new SdVolleyListener(MainActivity.this);
                lruCache = BitmapLruCache.open();
                try_count = 0;
                image_load_count=0;
                getData();
                restart_btn.setVisibility(View.GONE);
            }
        });

    }

    public void getData() {
        loading = ProgressDialog.show(this, "Please wait...", "Fetching data...", false, false);
        Log.e("SFFSDF", "sdfsdfs");


        final String url = "https://api.flickr.com/services/feeds/photos_public.gne";


        StringRequest getRequest = new StringRequest(Request.Method.GET, url, listener, listener);

        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(getRequest);

        getRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    private void showGrid() {
        //Looping through all the elements of json array
        //for (int i = 0; i < 3; i++) {
        //Creating a json object of the current index
        JSONObject obj = null;

        GridViewAdapter gridViewAdapter = new GridViewAdapter(this, images, names);

        //Adding adapter to gridview
        gridView.setAdapter(gridViewAdapter);


        // }
    }

    //    public void showImage(String uri) {
//        Dialog builder = new Dialog(this);
//        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        builder.getWindow().setBackgroundDrawable(
//                new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                //nothing;
//            }
//        });
//
//        ImageView imageView = new ImageView(this);
////        imageView.setImageURI(Uri.parse(uri));
//        Uri uri2 = Uri.parse("http://qan3.sdlcdn.com/imgs/a/a/d/large/BOK300440745_1380769766_image1-58b7e.jpg");
//        imageView.setImageURI(uri2);
//        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.MATCH_PARENT));
//        builder.show();
//    }
    public void showImage() {
//    final AlertDialog.Builder alertadd = new AlertDialog.Builder(
//            MainActivity.this);
//    alertadd.setTitle("Android");
//
//    LayoutInflater factory = LayoutInflater.from(MainActivity.this);
//    final View view = factoƒry.inflate(R.layout.popup, null);
//
//    ImageView image= (ImageView) view.findViewById(R.id.imageView);
//    image.setImageResource(R.drawable.app_icon);
//
////    TextView text= (TextView) view.findViewById(R.id.textView);
////    text.setText("Android");
//
//    alertadd.setView(view);
//    alertadd.setNeutralButton("ok", new DialogInterface.OnClickListener() {
//        public void onClick(DialogInterface dlg, int sumthin) {
//
//        }
//    });
//
//    alertadd.show();
        Collections.sort(exclusions);
        int randomnos = getRandomWithExclusion(rnd, exclusions);
        Log.e("Random", randomnos+"");
        exclusions.add(randomnos);
        Log.e("ExclusionSize", exclusions.size()+"");
        selected_item = randomnos;

        String uri = images.get(randomnos);
        ImageView imageView = new ImageView(getApplicationContext());
//    imageView.setImageResource(R.drawable.app_icon);
        ImageLoader imageLoader;
        NetworkImageView networkImageView = new NetworkImageView(context);
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        if(lruCache.get(uri) == null) {
            imageLoader.get(uri, getImageListener(networkImageView, R.mipmap.ic_launcher, android.R.drawable.ic_dialog_alert, uri));
        }else{
            networkImageView.setImageBitmap(lruCache.get(uri));
            Log.d("volley", "image already in cache show image");


        }
        Picasso.with(this).load(uri).into(imageView);
        dialog = new AlertDialog.Builder(this)
                .setView(imageView)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        if(this!= null) {
            dialog.show();
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.GONE);
            handlerThread = new Handler();
            handlerThread.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.VISIBLE);
                }
            }, 3000);
        }
    }
    public int getRandomWithExclusion(Random rnd, ArrayList<Integer>AI) {
        int start =0, end =8;
        for(int i=0;i<AI.size();i++){
            Log.e("EXclusion no", AI.get(i)+"");
        }

        int random = start + rnd.nextInt(end - start + 1 - AI.size());
        for (int ex : AI) {
            if (random < ex) {
                break;
            }
            random++;
        }
        return random;
    }


    public void stopProgress() {
        loading.dismiss();
    }

    final DefaultHandler handler = new DefaultHandler() {

        public void startElement(String uri, String localName, String qName,
                                 Attributes attributes) throws SAXException {
            if (qName.equalsIgnoreCase("link")) {
                String val = attributes.getValue(1);
                if (val.equalsIgnoreCase("image/jpeg") && images.size() < 9) {
                    images.add(attributes.getValue(2));
                    names.add("NAME");
                }
                if (images.size() == 9 && !shown) {
                    stopProgress();
                    showGrid();
                    shown = true;
                }


            }

        }//end of startElement method

        public void endElement(String uri, String localName,
                               String qName) throws SAXException {
        }

        public void characters(char ch[], int start, int length) throws SAXException {

        }//end of characters
    };//end of DefaultHandler object
    @Override
    public void onDestroy() {
        super.onDestroy();

        handlerThread.removeCallbacksAndMessages(null);

    }



    public  ImageLoader.ImageListener getImageListener(final ImageView view,
                                                       final int defaultImageResId, final int errorImageResId, final String uri) {
        return new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (errorImageResId != 0) {
                    view.setImageResource(errorImageResId);
                    image_load_count++;
                    Log.e("Response", "ON error response");
                    if(image_load_count==9){
                        ArrayList<String> temp;
                        temp = new ArrayList<>();
                        for (int j = 0; j < 9; j++) {
                            temp.add("http://www.ibtc.ir/images/icon/jpg.png");
                        }
                        Log.e("Handler", "handler started");
                        final GridViewAdapter adap_temp = new GridViewAdapter(context, temp, names);
                        handlerThread = new Handler();
                        handlerThread.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                gridView.setAdapter(adap_temp);
                                showImage();
                                try_count++;
                            }
                        }, 15000);
                    }
                }

            }

            @Override
            public void onResponse(final ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() != null) {
                    view.setImageBitmap(response.getBitmap());
                    image_load_count++;
                    Log.e("Response", "Bitmap not null");
                    if(image_load_count==9){
                        ArrayList<String> temp;
                        temp = new ArrayList<>();
                        for (int j = 0; j < 9; j++) {
                            temp.add("http://www.ibtc.ir/images/icon/jpg.png");
                        }
                        Log.e("Handler", "handler started");
                        new CountDownTimer(15000, 100) {

                            public void onTick(long millisUntilFinished) {
                                timer.setText(new SimpleDateFormat("ss").format(new Date( millisUntilFinished)) +" sec");
                            }

                            public void onFinish() {
                                timer.setText("Game Started!");
                                timerText.setVisibility(View.GONE);
                                restart_btn.setVisibility(View.GONE);
                            }
                        }.start();
                        final GridViewAdapter adap_temp = new GridViewAdapter(context, temp, names);
                        handlerThread = new Handler();
                        handlerThread.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                gridView.setAdapter(adap_temp);
                                showImage();
                                try_count++;
                            }
                        }, 15000);
                    }
                    lruCache.putBitmap(uri,response.getBitmap());
                } else if (defaultImageResId != 0) {
                    view.setImageResource(defaultImageResId);
                }

            }
        };
    }

}